<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'simak' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('WP_HOME','http://localhost/simak.id');
define('WP_SITEURL','http://localhost/simak.id');
define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'bsdygy|G)dZHZvG:w,N@`g*,ecnq>9kywk=|0ZbGp.VZY.4r$R:4z/Lb=SJ~gx.?' );
define( 'SECURE_AUTH_KEY',  'P(bJIvcZTajpkY213lk8p4SP}s(4BJQ- >==Dlx-HnSSc}Hn[(X91y*Jd{i-OG=;' );
define( 'LOGGED_IN_KEY',    'fs+%)|],lC|R*{F(oR+Y-t(wSdH%<C4tlrm^l%UvpPpRYT<fSE&W|I VjynL44WR' );
define( 'NONCE_KEY',        '<|}NB/(LgaoD1a `^=Gp,0m)738$I$Et^1?{u-JohKIzs3I(f0j7HajdSuOF7AwG' );
define( 'AUTH_SALT',        '<NC,,B[`ft649JqQX8ltO8746m^r7)8DZU)3l]s/W- pu]I]$`,EMY@Hd6l,Iim/' );
define( 'SECURE_AUTH_SALT', ']]j!o1X g;[]uW7AW4XY4 eqRO3E.h71A|n`}D1|F#e}LQ/zd5&O!!NVbJ0PK%$;' );
define( 'LOGGED_IN_SALT',   'IX<l[E+X[.$skz4D(zsmP&<<m~:`=s>7,{&ti3]]|faj^_p:Y6CP<w0n+oHF4>,J' );
define( 'NONCE_SALT',       'n2*t%-va`%O6-:[Pv0P-E{tP>=LdU82BT7p*;dNk~(r(5Q,doPdJS[p 9vAmT_3~' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
//Begin Really Simple SSL Load balancing fix
if ((isset($_ENV["HTTPS"]) && ("on" == $_ENV["HTTPS"]))
|| (isset($_SERVER["HTTP_X_FORWARDED_SSL"]) && (strpos($_SERVER["HTTP_X_FORWARDED_SSL"], "1") !== false))
|| (isset($_SERVER["HTTP_X_FORWARDED_SSL"]) && (strpos($_SERVER["HTTP_X_FORWARDED_SSL"], "on") !== false))
|| (isset($_SERVER["HTTP_CF_VISITOR"]) && (strpos($_SERVER["HTTP_CF_VISITOR"], "https") !== false))
|| (isset($_SERVER["HTTP_CLOUDFRONT_FORWARDED_PROTO"]) && (strpos($_SERVER["HTTP_CLOUDFRONT_FORWARDED_PROTO"], "https") !== false))
|| (isset($_SERVER["HTTP_X_FORWARDED_PROTO"]) && (strpos($_SERVER["HTTP_X_FORWARDED_PROTO"], "https") !== false))
|| (isset($_SERVER["HTTP_X_PROTO"]) && (strpos($_SERVER["HTTP_X_PROTO"], "SSL") !== false))
) {
   $_SERVER["HTTPS"] = "on";
}
//END Really Simple SSL
