<?php
/**
 * Initialize example plugin
 */
function xltab_admin_init() {

	// Initialize Sunrise
	$admin = new Sunrise7( array(
			// Sunrise file path
			'file' => __FILE__,
			// Plugin slug (should be equal to plugin directory name)
			'slug' => 'xltab',
			// Plugin prefix
			'prefix' => 'xltab',
			// Plugin textdomain
			'textdomain' => 'xltab',
			// Custom CSS assets folder
			'css' => '',
			// Custom JS assets folder
			'js' => '',
		) );

	// Prepare array with options
	$options = array(

		// Open tab: Regular fields
		array(
			'type' => 'opentab',
			'name' => __( 'Widgets', 'xltab' ),
		),
		array(
			'type' => 'openflex',
		),

		// Checkbox 
		array(
			'id'      => 'accordion',
			'type'    => 'checkbox',
			'default' => '',
			'name'    => __( 'Accordion', 'xltab' ),
			'pro'	  => '',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=accordion'	
		),

		array(
			'id'      => 'accordion-checkbox',
			'type'    => 'checkbox',
			'default' => 'on',
			'name'    => __( 'Accordion Checkbox', 'xltab' ),
			'pro'	  => '',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=accordion-checkbox'	
		),

		array(
			'id'      => 'image-accordion',
			'type'    => 'checkbox',
			'default' => 'on',
			'name'    => __( 'Image Accordion', 'xltab' ),
			'pro'	  => '',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=image-accordion'	
		),

		array(
			'id'      => 'tab1',
			'type'    => 'checkbox',
			'default' => 'on',
			'name'    => __( 'Tab', 'xltab' ),
			'pro'	  => '',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=tab'	
		),

		array(
			'id'      => 'tab-switch',
			'type'    => 'checkbox',
			'default' => 'on',
			'name'    => __( 'Switch Tab', 'xltab' ),
			'pro'	  => '',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=switch-tab'	
		),

		array(
			'id'      => 'tab-vertical',
			'type'    => 'checkbox',
			'default' => 'on',
			'name'    => __( 'Vertical Tab', 'xltab' ),
			'pro'	  => '',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=vertical-tab'	
		),

		array(
			'id'      => 'accordion-faq',
			'type'    => 'checkbox',
			'default' => '',
			'name'    => __( 'Faq Accordion', 'xltab' ),
			'pro'	  => 'yes',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=accordion-faq'	
		),

		array(
			'id'      => 'tab-app',
			'type'    => 'checkbox',
			'default' => '',
			'name'    => __( 'App Tab', 'xltab' ),
			'pro'	  => 'yes',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=app-tab'	
		),

		array(
			'id'      => 'tab-floating',
			'type'    => 'checkbox',
			'default' => '',
			'name'    => __( 'Floating Tab', 'xltab' ),
			'pro'	  => 'yes',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=floating-tab'	
		),

		array(
			'id'      => 'tab-steps',
			'type'    => 'checkbox',
			'default' => '',
			'name'    => __( 'Steps Tab', 'xltab' ),
			'pro'	  => 'yes',
			'preview' => 'http://plugins.webangon.com/khara/xltab/?elementor_library=tab-steps'	
		),

		array(
			'type' => 'closeflex',
		),

		// Close tab: Regular fields
		array(
			'type' => 'closetab',
		),
	);

	// Add sub-menu (like Dashboard -> Settings -> Permalinks)
	$admin->add_submenu( array(
			// Settings page <title>
			'page_title' => __( 'XlTab', 'xltab' ),
			// Menu title, will be shown in left dashboard menu
			'menu_title' => __( 'XlTab', 'xltab' ),
			// Unique page slug, you can use here the slug of parent page, which you've already created
			'slug' => 'xltab',
			// Slug of the parent page (see above)
			'parent_slug' => 'themes.php',
			// Array with options available on this page
			'options' => $options,
		) );
}

// Hook to plugins_loaded
add_action( 'plugins_loaded', 'xltab_admin_init' );
