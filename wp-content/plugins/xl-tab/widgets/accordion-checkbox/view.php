<?php

$uniqnm = 'd'.substr( $this->get_id_int(), 0, 3 );
  $slider_options = [
      'id' => substr( $this->get_id_int(), 0, 3 ),
  ];
$out = '';
$first=0;foreach ($settings['tabs'] as $a){ $first++;

    if($first==1){
        $actcls='class="active chkaccord '.$uniqnm.'"';
        $ckd = 'checked=""';
    } else {
        $actcls= 'class="chkaccord '.$uniqnm.'"';
        $ckd = '';
    }

    $title = $a['title']? '<input type="radio" name="'.$uniqnm.'" value="'.$a['title'].'" '.$ckd.'><span class="tbtitle">'.$a['title'].'</span><span class="accorcheckbx-checkmark"></span>'.$this->icon_image($a).'' : '';
    $out.= '<label '.$actcls.'>'.$title.'</label>';
}

 ?>

<?php echo '<div class="thepack-accorcheckbx" data-xld =\''.wp_json_encode($slider_options).'\'>';?>   
    <?php echo $out;?>
</div>


<style type="text/css">    
.thepack-accorcheckbx label {
    cursor: pointer;
    padding-left: 26px;
    display: block;
    margin-bottom: 15px;
    position: relative;
}
.thepack-accorcheckbx input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}
.thepack-accorcheckbx .accorcheckbx-checkmark {
    position: absolute;
    top: 11px;
    left: 0;
    height: 14px;
    width: 14px;
    border-radius: 50%;
    border: 1px solid #cdcdcd;
}
.accorcheckbx-checkmark {
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    height: 12px;
    width: 13px;
    border-radius: 2px;
    border: 1px solid #ccc;
    color: #000;
    font-size: 10px;
    font-weight: bolder;
}
.accorcheckbx-checkmark:after {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    display: none;
    content: '\f26b';
}

.thepack-accorcheckbx .accorcheckbx-checkmark:after {
    content: "";
    top: 6px;
    left: 6px;
    width: 6px;
    height: 6px;
    border-radius: 50%;
    background: #666666;
    position: absolute;
    display: none;
}
.thepack-accorcheckbx input:checked ~ .accorcheckbx-checkmark:after {
    display: block;
}
.thepack-accorcheckbx label.active .tooltip {
    display: block;
}
.thepack-accorcheckbx .tooltip {
    padding: 9px 22px;
    background: #f2f2f2;
    position: relative;
    margin-top: 16px;
    margin-bottom: 28px;
    display: none;
}
.thepack-accorcheckbx .tooltip:before {
    content: "";
    border-bottom: 10px solid #f2f2f2;
    border-right: 9px solid transparent;
    border-left: 9px solid transparent;
    position: absolute;
    bottom: 100%;
}
/*.thepack-accorcheckbx .tooltip {
  opacity: 1;
  animation-name: fadeInOpacity;
  animation-iteration-count: 1;
  animation-timing-function: ease-in;
  animation-duration: .3s;
}

@keyframes fadeInOpacity {
  0% {
    opacity: 0;
  }
  100% {
    opacity: 1;
  }
}*/
</style>