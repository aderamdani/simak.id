<?php

namespace XLTab\Widgets;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Border;
use Elementor\Scheme_Typography;
use Elementor\Utils;
use XLTab\xltab_helper;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class thepack_accor_checkbox extends Widget_Base {

    public function get_name() {
        return 'xlaccorcheck';
    }

    public function get_title() {
        return   esc_html__('ACCORDION CHECKBOX', 'xltab');
    } 
    
    public function get_icon() {
        return 'dashicons dashicons-layout';
    }


    protected function _register_controls() {

        $this->start_controls_section(
            'section_heading',
            [
                'label' =>   esc_html__('TAB', 'xltab'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'type', [
                'label' =>   esc_html__('Population', 'xltab'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'content' => [
                        'title' =>   esc_html__('Content', 'xltab'),
                        'icon' => ' eicon-document-file',
                    ],
                    'template' => [
                        'title' =>   esc_html__('Template', 'xltab'),
                        'icon' => 'eicon-image-rollover',
                    ]
                ],
                'default' => 'content',
                'label_block' => true,                
            ]
        );

        $repeater->add_control(
            'title', [
                'type' => Controls_Manager::TEXT,
                'label' =>   esc_html__('Label', 'xltab'),
                'label_block' => true,
                'default' =>'Car Insurance',
            ]
        );

        $repeater->add_control(
            'content', [
                'type' => Controls_Manager::WYSIWYG,
                'label' =>   esc_html__('Content', 'xltab'),
                'label_block' => true,
                'condition' => [
                    'type' => 'content',
                ],
            ]
        );

        $repeater->add_control(
            'template', [
                'type' => Controls_Manager::SELECT2,
                'options' => xltab_helper::xltab_drop_posts('elementor_library'),
                'multiple' => false,
                'label' =>   esc_html__('Template', 'xltab'),
                'label_block' => true,
                'condition' => [
                    'type' => 'template',
                ],
            ]
        );

                                
        $this->add_control(
            'tabs',
            [
                'type' => Controls_Manager::REPEATER,
                'prevent_empty' => false,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'title' =>   esc_html__( 'Finance', 'xltab' ),
                    ]
                ],
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_xgnr',
            [
                'label' =>   esc_html__('General', 'xltab'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );


        $this->add_responsive_control(
            'gheiht',
            [
                'label' =>   esc_html__( 'Item spacing', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'size_units' => ['px','em','%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    
                ],
                'selectors' => [
                    '{{WRAPPER}} .thepack-accorcheckbx label:not(:last-child)' => 'margin-bottom: {{SIZE}}{{UNIT}}',
                ],

            ]
        );

        $this->add_responsive_control(
            'glftpd',
            [
                'label' =>   esc_html__( 'Item left pading', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'size_units' => ['px','em','%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1000,
                        'step' => 1,
                    ],
                    
                ],
                'selectors' => [
                    '{{WRAPPER}} .thepack-accorcheckbx label' => 'padding-left: {{SIZE}}{{UNIT}}',
                ],

            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_gnrl',
            [
                'label' =>   esc_html__('Title', 'xltab'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        ); 


        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ttle',
                'selector' => '{{WRAPPER}} .tbtitle',
                'label' =>   esc_html__( 'Typo', 'xltab' ),
            ]
        );

        $this->add_control(
            'tclr',
            [
                'label' =>   esc_html__( 'Color', 'xltab' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .tbtitle' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();


        $this->start_controls_section(
            'section_styl1',
            [
                'label' =>   esc_html__('Checkbox', 'xltab'),
                'tab' => Controls_Manager::TAB_STYLE,                
            ]
        );

        $this->add_responsive_control(
            'ckwid',
            [
                'label' =>   esc_html__( 'Width & height', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark' => 'width: {{SIZE}}px;height: {{SIZE}}px;',
                ],

            ]
        );

        $this->add_responsive_control(
            'ckbdr',
            [
                'label' =>   esc_html__( 'Border radius', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark' => 'border-radius: {{SIZE}}px;',
                ],

            ]
        );

        $this->add_responsive_control(
            'cktsp',
            [
                'label' =>   esc_html__( 'Top spacing', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark' => 'top: {{SIZE}}px;',
                ],

            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'ckybdr',
                'label' =>   esc_html__( 'Border', 'xltab' ),
                'selector' => '{{WRAPPER}} .accorcheckbx-checkmark',
            ]
        );

        $this->add_responsive_control(
            'ckswid',
            [
                'label' =>   esc_html__( 'Dot Width & height', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark:after' => 'width: {{SIZE}}px;height: {{SIZE}}px;',
                ],

            ]
        );

        $this->add_responsive_control(
            'cksbdr',
            [
                'label' =>   esc_html__( 'Dot Border radius', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark:after' => 'border-radius: {{SIZE}}px;',
                ],

            ]
        );

        $this->add_responsive_control(
            'ckstsp',
            [
                'label' =>   esc_html__( 'Dot Top spacing', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark:after' => 'top: {{SIZE}}px;',
                ],

            ]
        );

        $this->add_responsive_control(
            'ckslsp',
            [
                'label' =>   esc_html__( 'Dot Left spacing', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark:after' => 'left: {{SIZE}}px;',
                ],

            ]
        );

        $this->add_control(
            'ckbg',
            [
                'label' =>   esc_html__( 'Dot background', 'xltab' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .accorcheckbx-checkmark:after' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_content',
            [
                'label' =>   esc_html__('Content', 'xltab'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'cntbg',
            [
                'label' =>   esc_html__( 'Background', 'xltab' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [ 
                    '{{WRAPPER}} .tooltip' => 'background: {{VALUE}};',
                    '{{WRAPPER}} .thepack-accorcheckbx .tooltip:before' => 'border-bottom-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
          'cntpd',
          [
             'label' =>   esc_html__( 'Padding', 'xltab' ),
             'type' => Controls_Manager::DIMENSIONS,
             'size_units' => [ 'px','em'],
             'selectors' => [
                    '{{WRAPPER}} .tooltip' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
             ],
          ]
        );

        $this->add_responsive_control(
          'cntmg',
          [
             'label' =>   esc_html__( 'Margin', 'xltab' ),
             'type' => Controls_Manager::DIMENSIONS,
             'size_units' => [ 'px','em'],
             'selectors' => [
                    '{{WRAPPER}} .tooltip' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
             ],
          ]
        );

        $this->add_control(
            'cntclr',
            [
                'label' =>   esc_html__( 'Color', 'xltab' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .tooltip' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ctle',
                'selector' => '{{WRAPPER}} .tooltip',
                'label' =>   esc_html__( 'Typo', 'xltab' ),
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        require dirname(__FILE__) .'/view.php';
    }

    private function icon_image($icon) { 

        $type = $icon['type'];
        if ($type == 'template'){
            return '<div class="tooltip">'.do_shortcode('[XLTAB_INSERT_TPL id="'.$icon['template'].'"]').'</div>';
        } else {
            return '<div class="tooltip">'.$icon['content'].'</div>';
        } 

    }

}

$widgets_manager->register_widget_type(new \XLTab\Widgets\thepack_accor_checkbox());