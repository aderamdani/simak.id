<?php
$out = ''; 
foreach ($settings['tabs'] as $a){

    $title = $a['title']? '<h2 class="title">'.$a['title'].'</h2>' : '';
    $sub = $a['sub']? '<p class="sub">'.$a['sub'].'</p>' : '';

    $url = $a['url']['url'];
    $ext = $a['url']['is_external'];
    $nofollow = $a['url']['nofollow'];
    $url = ( isset($url) && $url ) ? 'href='.esc_url($url). '' : '';
    $ext = ( isset($ext) && $ext ) ? 'target= _blank' : '';
    $nofollow = ( isset($url) && $url ) ? 'rel=nofollow' : '';
    $link = $url.' '.$ext.' '.$nofollow;
    $murl = $url ? '<a class="hoverlink" '.$ext.' href="'.esc_url($a['url']['url']).'" '.$nofollow.'>' : '<a href="#">';

    $bg = $a['img']['id'] ? 'style="background:url('.wp_get_attachment_image_url($a['img']['id'],'full').');"' : '';

    $out.='
      <li '.$bg.'>
        <div class="inr">'.$murl.'
          '.$title.$sub.'
          </a></div>
      </li>
    ';

}

 ?>

<div class="xldhvracd">
  <ul>
    <?php echo $out;?>
  </ul>
</div>

<style type="text/css">    
.xldhvracd {
  width: 100%;
  height: 450px;
  overflow: hidden;
}

.xldhvracd ul {
  width: 100%;
  display: table;
  table-layout: fixed;
  margin: 0;
  padding: 0;
}

.xldhvracd ul li {
  display: table-cell;
  vertical-align: bottom;
  position: relative;
  width: 16.666%;
  height: 450px;
  background-repeat: no-repeat;
  background-position: center center;
  transition: all 500ms ease;
}

.xldhvracd ul li div {
    display: block;
    overflow: hidden;
    width: 100%;
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
}

.xldhvracd ul li div a {
    display: block;
    width: 100%;
    position: absolute;
    bottom: 0;
    top: 0;
    z-index: 3;
    text-decoration: none;
    transition: all 200ms ease;   
}

.xldhvracd ul li div a * {
  opacity: 0;
  margin: 0;
  width: 100%;
  text-overflow: ellipsis;
  position: relative;
  z-index: 5;
  white-space: nowrap;
  overflow: hidden;
  -webkit-transform: translateX(-20px);
  transform: translateX(-20px);
  transition: all 400ms ease;
}

.xldhvracd ul li div a h2 {
  color: #fff;
  text-overflow: clip;
  letter-spacing: 1px;
  font-size: 24px;
  text-transform: uppercase;
  margin-bottom: 2px;
  top: 160px;
}

.xldhvracd ul li div a p {
  top: 160px;
  font-size: 13.5px;
}
.xldhvracd ul:hover li { width: 8%; }

.xldhvracd ul:hover li:hover { width: 60%; }

.xldhvracd ul:hover li:hover a { background: rgba(0, 0, 0, 0.4); }

.xldhvracd ul:hover li:hover a * {
  opacity: 1;
  -webkit-transform: translateX(0);
  transform: translateX(0);
}

@media screen and (max-width: 600px) {

  .xldhvracd { height: auto !important; }

  .xldhvracd ul li,
  .xldhvracd ul li:hover,
  .xldhvracd ul:hover li,
  .xldhvracd ul:hover li:hover {
    position: relative;
    display: table;
    table-layout: fixed;
    width: 100%;
    transition: none;
  }
  .xldhvracd ul li div a * {
    opacity: 0;
    -webkit-transform: translateX(0);
    transform: translateX(0);
  }

}
.xldhvracd ul li:hover div{
      background: transparent !important;
}
.xldhvracd ul li:first-child div{
  border-left-width:0px !important;
}
</style>