<?php

namespace XLTab\Widgets;
use Elementor\Widget_Base;
use Elementor\Controls_Manager;
use Elementor\Scheme_Color;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Background;
use Elementor\Scheme_Typography;
use Elementor\Utils;
use XLTab\xltab_helper;

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly


class thepack_imgaccordion_xl extends Widget_Base {

    public function get_name() {
        return 'xlimgaccordion';
    }

    public function get_title() {
        return   esc_html__('IMG ACCORDION', 'xltab');
    } 
    
    public function get_icon() {
        return 'dashicons dashicons-unlock';
    }


    protected function _register_controls() {

        $this->start_controls_section(
            'section_heading',
            [
                'label' =>   esc_html__('TAB', 'xltab'),
            ]
        );

        $repeater = new \Elementor\Repeater();

        $repeater->add_control(
            'title', [
                'type' => Controls_Manager::TEXT,
                'label' =>   esc_html__('Title', 'xltab'),
                'label_block' => true,
                'default' =>'Car Insurance',
            ]
        );

        $repeater->add_control(
            'sub', [
                'type' => Controls_Manager::TEXTAREA,
                'label' =>   esc_html__('Sub title', 'xltab'),
                'label_block' => true,
            ]
        );

        $repeater->add_control(
            'img', [
                'label' =>   esc_html__( 'Background image', 'xltab' ),
                'type' => Controls_Manager::MEDIA,
                'default' => [
                    'url' => Utils::get_placeholder_image_src(),
                ],
                'label_block' => true,
            ]
        );      

        $repeater->add_control(
            'url', [
                'label' =>   esc_html__('Link', 'xltab'),
                'type' => Controls_Manager::URL,
                'label_block' => true,
                'placeholder' =>   esc_html__('http://your-link.com', 'xltab'),
            ]
        );

        $this->add_control(
            'tabs',
            [
                'type' => Controls_Manager::REPEATER,
                'prevent_empty' => false,
                'fields' => $repeater->get_controls(),
                'default' => [
                    [
                        'title' =>   esc_html__( 'Finance', 'xltab' ),
                    ]
                ],
                'title_field' => '{{{ title }}}',
            ]
        );

        $this->end_controls_section();

        $this->start_controls_section(
            'section_xgnr',
            [
                'label' =>   esc_html__('General', 'xltab'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        ); 

        $this->add_responsive_control(
            'gmawid',
            [
                'label' =>   esc_html__( 'Height', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'size_units' => ['px','%','vh'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 1500,
                        'step' => 1,
                    ],
                    
                ],
                'selectors' => [
                    '{{WRAPPER}} .xldhvracd,{{WRAPPER}} .xldhvracd ul li' => 'height: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->add_responsive_control(
            'glfbdr',
            [
                'label' =>   esc_html__( 'Left border width', 'xltab' ),
                'type' =>  Controls_Manager::SLIDER,
                'selectors' => [
                    '{{WRAPPER}} .xldhvracd ul li div' => 'border-left-width: {{SIZE}}{{UNIT}};border-left-style:solid;',
                ],

            ]
        );

        $this->add_control(
            'glfclr',
            [
                'label' =>   esc_html__( 'Left border color', 'xltab' ),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .xldhvracd ul li div' => 'border-left-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
          'gmpdg',
          [
             'label' =>   esc_html__( 'Text padding', 'xltab' ),
             'type' => Controls_Manager::DIMENSIONS,
             'size_units' => [ 'px','em'],
             'selectors' => [
                    '{{WRAPPER}} .xldhvracd ul li div a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
             ],
          ]
        );

        $this->start_controls_tabs('ghve');

        $this->start_controls_tab(
            'actstl',
            [
                'label' => __( 'Normal', 'elementor' ),
            ]
        );

        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'gnbg',
                'label' =>   esc_html__( 'Background', 'elementor' ),
                'types' => [ 'none', 'classic','gradient' ],
                'selector' => '{{WRAPPER}} .xldhvracd ul li div',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'actnrml',
            [
                'label' => __( 'Hover', 'elementor' ),
            ]
        );


        $this->add_group_control(
            Group_Control_Background::get_type(),
            [
                'name' => 'ghbg',
                'label' =>   esc_html__( 'Background', 'elementor' ),
                'types' => [ 'none', 'classic','gradient' ],
                'selector' => '{{WRAPPER}} .xldhvracd ul:hover li:hover a',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

                
        $this->end_controls_section();

        $this->start_controls_section(
            'section_cnty',
            [
                'label' =>   esc_html__('Content', 'xltab'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
          'tpady',
          [
             'label' =>   esc_html__( 'Title margin', 'xltab' ),
             'type' => Controls_Manager::DIMENSIONS,
             'size_units' => [ 'px','em'],
             'selectors' => [
                    '{{WRAPPER}} .title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
             ],
          ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'nvtre',
                'selector' => '{{WRAPPER}} .title',
                'label' =>   esc_html__( 'Title typo', 'xltab' ),
            ]
        );

        $this->add_control(
            'ikclr',
            [
                'label' =>   esc_html__('Title Color', 'xltab'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .title' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'snvtre',
                'selector' => '{{WRAPPER}} .sub',
                'label' =>   esc_html__( 'Sub Title typo', 'xltab' ),
            ]
        );

        $this->add_control(
            'sikclr',
            [
                'label' =>   esc_html__('Sub Title Color', 'xltab'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .sub' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render() {

        $settings = $this->get_settings();
        require dirname(__FILE__) .'/view.php';
    }

}

$widgets_manager->register_widget_type(new \XLTab\Widgets\thepack_imgaccordion_xl());